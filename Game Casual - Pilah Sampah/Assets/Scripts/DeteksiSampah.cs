﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeteksiSampah : MonoBehaviour
{
    public string nameTag;
    public AudioClip audioBenar;
    public AudioClip audioSalah;
    private AudioSource MediaPlayerBenar;
    private AudioSource MediaPlayerSalah;
    public Text textScore;

    // Start is called before the first frame update
    void Start()
    {
        // Play audio "Benar" if the answer is correct
        MediaPlayerBenar = gameObject.AddComponent<AudioSource>();
        MediaPlayerBenar.clip = audioBenar;

        // Play audio "Salah" if the answer is wrong
        MediaPlayerSalah = gameObject.AddComponent<AudioSource>();
        MediaPlayerSalah.clip = audioSalah;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // The score will increase by 10 for object which placed to it's correct place
        if (collision.tag.Equals(nameTag))
        {
            Data.score += 10;
            textScore.text = Data.score.ToString();
            Destroy(collision.gameObject);
            MediaPlayerBenar.Play();
        }
        else
        {
            // The score will be deducted by 15 for undesirable object
            Data.score -= 15;
            textScore.text = Data.score.ToString();
            Destroy(collision.gameObject);
            MediaPlayerSalah.Play();

            // Game ends when the score reaches 0 or bellow, then proceed to show "GameOver" scene
            if (Data.score <= 0)
            {
                SceneManager.LoadScene("GameOver");
            }
        }
    }
}
